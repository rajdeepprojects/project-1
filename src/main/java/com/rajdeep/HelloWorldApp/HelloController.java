package com.rajdeep.HelloWorldApp;
import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {
	@RequestMapping("/hello")
	public String hello()
	{
		return "Hello World";
	}
}
